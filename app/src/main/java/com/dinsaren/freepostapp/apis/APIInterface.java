package com.dinsaren.freepostapp.apis;


import com.dinsaren.freepostapp.models.Category;
import com.dinsaren.freepostapp.models.Product;
import com.dinsaren.freepostapp.models.ProductResponse;
import com.dinsaren.freepostapp.ui.auth.models.LoginRequest;
import com.dinsaren.freepostapp.ui.auth.models.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {
    @GET("/api/admin/category/list")
    Call<List<Category>> getAllCategory(@Header("Authorization") String auth);
    @POST("/api/admin/category/create")
    Call<Void> createCategory(@Body Category responseItem);

    @GET("/api/admin/category/{id}")
    Call<Category> getCategoryById(@Path("id") Integer id);

    @POST("/api/admin/category/update")
    Call<Void> updateCategory(@Body Category responseItem);

    @POST("/api/admin/category/delete")
    Call<Void> deleteCategory(@Body Category responseItem);

    @GET("/api/admin/product/list")
    Call<List<Product>> getProducts(@Header("Authorization") String auth);

    @POST("/api/admin/product/create")
    Call<Void> createProduct(@Body Product req);

    @GET("/api/admin/product/{id}")
    Call<Product> getProductById(@Path("id") Integer id);

    @POST("/api/oauth/token")
    Call<LoginResponse> login(@Body LoginRequest req);

}
