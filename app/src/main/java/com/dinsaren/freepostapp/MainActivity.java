package com.dinsaren.freepostapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.dinsaren.freepostapp.apis.APIInterface;
import com.dinsaren.freepostapp.app.BaseActivity;
import com.dinsaren.freepostapp.data.local.UserSharePreference;
import com.dinsaren.freepostapp.ui.auth.LoginActivity;
import com.dinsaren.freepostapp.ui.category.CategoryActivity;
import com.dinsaren.freepostapp.ui.product.ProductActivity;

public class MainActivity extends BaseActivity {
    private APIInterface apiInterface;
    private Button btnOpenCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnOpenCategory = findViewById(R.id.btnListCategory);
        btnOpenCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
                startActivity(intent);
            }
        });
    }

    public void openProductActivity(View view) {
        Intent intent = new Intent(MainActivity.this, ProductActivity.class);
        startActivity(intent);
    }

    public void onClickLogout(View view){
        UserSharePreference.clearUser(this);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}