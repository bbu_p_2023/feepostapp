package com.dinsaren.freepostapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.freepostapp.R;
import com.dinsaren.freepostapp.models.Category;
import com.dinsaren.freepostapp.ui.category.FormCategoryActivity;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private List<Category> categoryList;
    private Context context;
    private OnClickListener onClickListener;

    public CategoryAdapter(List<Category> categoryList, Context context, OnClickListener onClickListener) {
        this.categoryList = categoryList;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_card_item_layout, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category item = categoryList.get(position);
        if (item != null) {
            holder.name.setText(item.getName());
            if(item.getStatus().equals("DEL")){
                holder.status.setText("Delete");
                holder.status.setTextColor(context.getResources().getColor(R.color.red));
            }else{
                holder.status.setText("Active");
                holder.status.setTextColor(context.getResources().getColor(R.color.purple_200));
            }
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, FormCategoryActivity.class);
                    intent.putExtra("ID", item.getId());
                    context.startActivity(intent);
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onClick(view, item);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView image, edit, delete;
        TextView name, status;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.ivCategoryImage);
            edit = itemView.findViewById(R.id.ivEdit);
            delete = itemView.findViewById(R.id.ivDelete);
            name = itemView.findViewById(R.id.tvCategoryName);
            status =itemView.findViewById(R.id.status);
        }
    }

    public  interface OnClickListener{
        void onClick(View view, Category item);
    }
}
