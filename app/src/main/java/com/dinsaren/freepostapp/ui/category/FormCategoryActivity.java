package com.dinsaren.freepostapp.ui.category;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.dinsaren.freepostapp.R;
import com.dinsaren.freepostapp.apis.APIClient;
import com.dinsaren.freepostapp.apis.APIInterface;
import com.dinsaren.freepostapp.app.BaseBackButtonActivity;
import com.dinsaren.freepostapp.models.Category;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormCategoryActivity extends BaseBackButtonActivity {
    private Button btnCreate;
    private EditText editName, editNameKh;
    private ProgressBar progressBar;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_category);
        initView();
    }

    private void initView() {
        setTitle("Create Category");
        btnCreate = findViewById(R.id.btnCreate);
        editName = findViewById(R.id.editCategoryName);
        editNameKh = findViewById(R.id.editCategoryNameKhmer);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Category item = new Category();
        Intent intent = getIntent();
        Integer id = intent.getIntExtra("ID", 0);
        if (id != 0) {
            setTitle("Update Category");
            btnCreate.setText("Update");
            progressBar.setVisibility(View.VISIBLE);
            apiInterface.getCategoryById(id).enqueue(new Callback<Category>() {
                @Override
                public void onResponse(Call<Category> call, Response<Category> response) {
                    if (response.isSuccessful()) {
                        editName.setText(response.body().getName().toString());
                        editNameKh.setText(response.body().getNameKh());
                        item.setId(response.body().getId());
                        item.setName(response.body().getName());
                        item.setNameKh(response.body().getNameKh());
                        item.setStatus(response.body().getStatus());
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<Category> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editName.getText().toString();
                String nameKh = editNameKh.getText().toString();
                if (name.equals("")) {
                    showToastMessage("Category Name is required");
                    return;
                }
                if (nameKh.equals("")) {
                    showToastMessage("Category Name Khmer is required");
                    return;
                }

                item.setStatus("ACT");
                item.setName(name);
                item.setNameKh(nameKh);
                progressBar.setVisibility(View.VISIBLE);
                if (item.getId() != 0) {
                    apiInterface.updateCategory(item).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            progressBar.setVisibility(View.GONE);
                            if (response.isSuccessful()) {
                                showToastMessage("Update Success");
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            showToastMessage(t.getMessage());
                        }
                    });
                } else {
                    apiInterface.createCategory(item).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            progressBar.setVisibility(View.GONE);
                            if (response.isSuccessful()) {
                                showToastMessage("Create Success");
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            showToastMessage(t.getMessage());
                        }
                    });
                }

            }
        });
    }
}